var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var kanalGesla = {'Skedenj': false};

exports.listen = function (streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    spremembaKanala(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function () {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajZasebnoSporocilo(socket);
    obdelajSeznamUporabnikov(trenutniKanal[socket.id]);
    socket.on('disconnect', function(){
      socket.leave(trenutniKanal[socket.id]);
      obdelajSeznamUporabnikov(trenutniKanal[socket.id]);
    })
  });
};

function obdelajZasebnoSporocilo(socket){
  socket.on('zasebno', function(data){
    var idUporabnika;
    for (var id in vzdevkiGledeNaSocket){
      if (vzdevkiGledeNaSocket.hasOwnProperty(id)){
        if (vzdevkiGledeNaSocket[id] == data.vzdevek){
          idUporabnika = id;
          break;
        }
      }
    }
    if (typeof idUporabnika === 'undefined' || idUporabnika == socket.id){
      socket.emit('zasebnoNapaka', data);
      return;
    }
    io.sockets.socket(idUporabnika).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ' (zasebno): ' + data.besedilo
    });
  })
}

function obdelajSeznamUporabnikov(kanal){
  var uporabniki = [];
  var naKanalu = io.sockets.clients(kanal);
  for (var i in naKanalu){
    uporabniki.push(vzdevkiGledeNaSocket[naKanalu[i].id]);
  }
  io.sockets.to(kanal).emit('uporabnikiOdgovor', {
    uporabniki: uporabniki
  });
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function spremembaKanala(socket, kanal, geslo) {
  geslo = geslo || false;
  if (kanalGesla[kanal]) {
    if (kanalGesla[kanal] != geslo) {
      socket.emit('pridruzitevOdgovor', {
          uspesno: false,
          sporocilo: 'Pridružitev v kanal ' + kanal + ' ni bila uspešna, ker je geslo napačno!'
        }
      );
      return;
    }
  }
  if (kanalGesla[kanal] === false && geslo) {
    socket.emit('pridruzitevOdgovor', {
        uspesno: false,
        sporocilo: 'Izbrani kanal ' + kanal + ' je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite' +
        ' z uporabo /pridruzitev ' + kanal + ' ali zahtevajte kreiranje kanala z drugim imenom.'
      }
    );
    return;
  }

  socket.leave(trenutniKanal[socket.id]);
  socket.join(kanal);
  kanalGesla[kanal] = geslo;
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal, uspesno: true});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function (vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
    obdelajSeznamUporabnikov(trenutniKanal[socket.id]);
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function (kanal) {
    var prejsnjiKanal = trenutniKanal[socket.id];
    socket.leave(trenutniKanal[socket.id]);
    spremambaKanala(socket, kanal.novKanal, kanal.geslo);
    obdelajSeznamUporabnikov(prejsnjiKanal);
    obdelajSeznamUporabnikov(trenutniKanal[socket.id]);
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function () {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}