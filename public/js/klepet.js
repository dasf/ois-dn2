var Klepet = function (socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function (kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.procesirajUkaz = function (ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch (ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal, geslo;
      var tekst = besede.join(' ');
      var pattern = /"([^"]*)"/g;

      if (tekst[0] !== '"') {
        kanal = tekst;
      }
      else {
        kanal = pattern.exec(tekst);
        geslo = pattern.exec(tekst);
        if (!(kanal && geslo)) {
          sporocilo = 'Neznan ukaz.';
          break;
        }
        kanal = kanal[1];
        geslo = geslo[1];
      }

      this.socket.emit('pridruzitevZahteva', {
        novKanal: kanal,
        geslo: geslo
      });
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var ime = besede.shift();
      var besedilo = besede.join(' ');
      if (!(ime && besedilo && ime.match(/^".*"$/) && besedilo.match(/^".*"$/))) {
        sporocilo = 'Neznan ukaz.';
        break;
      }
      this.socket.emit('zasebno', {
        vzdevek: ime.replace(/^"|"$/g, ''),
        besedilo: besedilo.replace(/^"|"$/g, '')
      });
      sporocilo = '(zasebno za ' + ime.replace(/^"|"$/g, '') + '): ' + besedilo;
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  }

  return sporocilo;
};