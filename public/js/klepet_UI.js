function divElementEnostavniTekst(sporocilo) {
  var url = 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/';
  var smeski = {
    ';)': 'wink.png',
    ':)': 'smiley.png',
    '(y)': 'like.png',
    ':*': 'kiss.png',
    ':(': 'sad.png'
  };
  var htmlEscaped = $('<div/>').text(zamenjajBesede(sporocilo)).html();

  function escapeRegex(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
  }

  for (var koda in smeski) {
    if (smeski.hasOwnProperty(koda)) {
      htmlEscaped = htmlEscaped.replace(new RegExp(escapeRegex(koda), 'g'), '<img src="' + url + smeski[koda] + '"/>');
    }
  }
  return $('<div style="font-weight: bold"></div>').html('<i>' + htmlEscaped + '</i>');
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function zamenjajBesede(sporocilo) {

  function _zamenjaj(data, _sporocilo){
    $.each(data.split('\r\n'), function (index, beseda) {
      _sporocilo = _sporocilo.replace(
        new RegExp('(\\b)' + beseda + '(\\b)', 'gi'),
        '$1' + new Array(this.length + 1).join('*') + '$2');
    });
    return _sporocilo;
  }

  if (window.swearWords){
    return _zamenjaj(window.swearWords, sporocilo);
  }

  $.ajax('/swearWords.txt', {method: 'GET', async: false}).done(function (response) {
    window.swearWords = response;
    return _zamenjaj(window.swearWords, sporocilo);
  });
}


function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    klepetApp.posljiSporocilo(klepetApp.kanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function () {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function (rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      klepetApp.vzdevek = rezultat.vzdevek;
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#kanal').text(klepetApp.vzdevek + ' @ ' + klepetApp.kanal);
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    if (rezultat.uspesno){
      klepetApp.kanal = rezultat.kanal;
      $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
      $('#kanal').text(klepetApp.vzdevek + ' @ ' + klepetApp.kanal)
    }
    else {
      $('#sporocila').append(divElementHtmlTekst(rezultat.sporocilo));
    }
  });
  socket.on('zasebnoNapaka', function(rezultat){
    var sporocilo = 'Sporočila "' + rezultat.besedilo +
      '" uporabniku z vzdevkom "' + rezultat.vzdevek + '" ni bilo mogoče posredovati.';
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('sporocilo', function (sporocilo) {
    $('#sporocila').append(divElementEnostavniTekst(sporocilo.besedilo));
  });

  socket.on('kanali', function (kanali) {
    $('#seznam-kanalov').empty();

    for (var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function () {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function () {
    socket.emit('kanali');
  }, 1000);

  socket.on('uporabnikiOdgovor', function(data){
    $('#seznam-uporabnikov').empty();
    $.each(data.uporabniki, function(){
      $('#seznam-uporabnikov').append(divElementHtmlTekst(this));
    })
  });

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function () {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});